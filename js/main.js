const request = new XMLHttpRequest();

request.open("GET", "../person.json", true);
request.addEventListener('load', () => {

    // Get data from json file
    let my_JSON_object = JSON.parse(request.responseText);
    let { metaData, rows } = my_JSON_object.data;

    let headers = [];
    let newObj = {}
    let persones = [];


    // Create asrray for headers
    metaData.forEach(element => {
        let {name} = element;
        headers.push(name);
    });

    rows.unshift(headers);


    // Create new object of persones
    for(let i = 1; i < rows.length; i++) {
        rows[0].forEach((value, index) => {

            newObj[value] = rows[i][index];
    
        });
        persones.push(newObj);
        newObj = {};
    };


    // Create blocks for blocks view
    let blocksWrapper = document.querySelector('.blocks-wrapper');

    persones.forEach((person, index) => {
        let personBlock = document.createElement('div');
        personBlock.classList.add('person-i');

        for(let info in person) {
            let infoRow = document.createElement('div');
            let infoField = document.createElement('span');
            let infoVal = document.createElement('span');

            infoRow.classList.add('info-row');
            infoField.classList.add('info-header');

            infoField.innerText = `${info}: `;
            infoVal.innerText = person[info];
            
            infoRow.appendChild(infoField);
            infoRow.appendChild(infoVal);
            personBlock.appendChild(infoRow);
        }

        blocksWrapper.appendChild(personBlock);
    });


    // Create table
    let wrapper = document.querySelector('#wrapper');
    let table = document.createElement('table');

    table.setAttribute('align', 'center');
    table.id = 'personTable';
    table.classList.add('personTable');


    // Create table head
    const tableHead = document.createElement('thead');
    const tableHeadRow = document.createElement('tr');

    const HeadersArray = [];

    for(let item in persones[0]){
        const tableHeadData = document.createElement('th');
        tableHeadData.textContent = item;
        tableHeadRow.appendChild(tableHeadData);
        HeadersArray.push(item);
    };

    const tableHeadButton = document.createElement('th');
    tableHeadButton.classList.add('button-wrapp')
    tableHeadRow.appendChild(tableHeadButton);


    // Create table body
    const tableBody = document.createElement('tbody');

    let tableRow = (index = 0, elem = '', arrayLength = 0) => {
        const tableBodyRow = document.createElement('tr');

        if ( elem ) { // For creating table data from existing data
            for(let item in elem){
                tableData(elem, item, tableBodyRow);
            }
        } else { // For creating new table data with empty data
            for(let i = 0; i < arrayLength; i++) {
                tableData(null, null, tableBodyRow);
            }

        }


        // close input popup
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            if ( evt.key === 'Escape' || evt.key === 'Enter' ) {
                let activeRows = document.getElementsByClassName('active')[0];
                activeRows.classList.remove('active');
            }
        };


        // Create buttons for remove rows
        const tableButtonBlock = document.createElement('td');
        const buttonRemove = document.createElement('a');

        tableButtonBlock.classList.add('button-wrapp');
        buttonRemove.textContent = 'Удалить';
        buttonRemove.classList.add('btn');


        tableButtonBlock.appendChild(buttonRemove);
        tableBodyRow.appendChild(tableButtonBlock);
        tableBody.appendChild(tableBodyRow);

        buttonRemove.addEventListener('click', e => {
            let dataRow = e.target.closest('tr');
            let cardNumber = dataRow.firstElementChild.textContent;

            persones.splice(persones.findIndex(v => v.CARD === cardNumber), 1);
            
            dataRow.remove();
        });
    };

    
    // Create table data function
    let tableData = (elem = null, item = null, tableBodyRow = '') => {
        const tableBodyData = document.createElement('td');
        const tableText = document.createElement('span');
        const tableinput = document.createElement('input');
        const inputWrapper = document.createElement('div');
        const tableHeadName = document.createElement('h2');

        inputWrapper.classList.add('input-wrapper');
        inputWrapper.appendChild(tableHeadName);
        inputWrapper.appendChild(tableinput);

        if (elem) {
            tableText.textContent = elem[item];
        } else {
            tableText.textContent = 'click on me!';
            tableText.classList.add('tmp-text')
        }

        tableText.classList.add('text');


        // Show and hide add and change data in table
        tableBodyData.addEventListener('click', e => {
            let input = '';
            let wrapperBlock = '';
            let cellNumber = '';

            if (e.target.tagName === 'TD') {
                input = e.target.getElementsByTagName('input')[0];
                wrapperBlock = e.target;
                cellNumber = e.target.cellIndex;
            } else if (e.target.tagName === 'SPAN') {
                input = e.target.parentNode.getElementsByTagName('input')[0];
                wrapperBlock = e.target.parentNode;
                cellNumber = e.target.parentNode.cellIndex;
            }

            tableHeadName.innerText = HeadersArray[cellNumber];

            input.value = tableText.innerText;

            if ( e.target.tagName != 'INPUT' && wrapperBlock.className === '' ) {
                wrapperBlock.classList.add('active');

                if ( tableText.innerText === 'click on me!' ) {
                    input.value = '';
                }

                input.focus();
                
            } else if ( e.target.tagName != 'INPUT' && wrapperBlock.className === 'active' ) {
                wrapperBlock.classList.remove('active');
            } else {
                e.preventDefault();
            }

            input.addEventListener('blur', (e) => {
                if ( input.value.length ) {
                    tableText.innerText = input.value;
                }
                e.target.closest('td').classList.remove('active');
            });
        });

        tableBodyData.appendChild(tableText);
        tableBodyData.appendChild(inputWrapper);
        tableBodyRow.appendChild(tableBodyData);
    };
    

    Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    
    var objectSize = Object.size(persones[0]);
    

    // Create table data
    persones.forEach((elem, index) => {
        tableRow(index, elem);
    });


    // Create button for adding new row
    const buttonAdd = document.createElement('a');
    buttonAdd.textContent = 'Добавить';
    buttonAdd.classList.add('btn');

    buttonAdd.addEventListener('click', () => {
        tableRow(0, '', objectSize);
    });


    // Add table to document
    tableHead.appendChild(tableHeadRow);
    table.appendChild(tableHead);
    table.appendChild(tableBody);
    wrapper.appendChild(table);
    wrapper.appendChild(buttonAdd);
});

request.send();